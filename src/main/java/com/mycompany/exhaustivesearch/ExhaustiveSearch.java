/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.exhaustivesearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Aritsu
 */
public class ExhaustiveSearch {

    public static int max(int a, int b) {
        if (a > b) {
            return a;
        }
        return b;
    }

    public static int knapSack(int cap, int weight[], int value[], int n) {
        if (n == 0 || cap == 0) {
            return 0;
        }
        if (weight[n - 1] > cap) {
            return knapSack(cap, weight, value, n - 1);
        }else{
            return max(value[n-1] + knapSack(cap - weight[n-1] , 
                    weight,value,n-1), 
                    knapSack(cap , weight , value ,n-1));
        }
    }

    public static void main(String[] args) {
        int weight[] = new int[] {2,5,10,5};
        int value[] = new int[] {20,30,50,10};
        int cap = 16;
        int n = value.length;
        System.out.println("The most valuable subset of the items that fit into knapsack is: ");
        System.out.println(knapSack(cap,weight,value,n));
        
    }

}
